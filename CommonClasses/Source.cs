﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CommonClasses
{
    public abstract class Source
    {
        // User name for login
        public abstract string LoginUserName { get; set; }
        // User password for login
        public abstract string LoginUserPassword { get; set; }
    }
}
