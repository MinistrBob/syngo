﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CommonClasses
{
    public class Job
    {
        // Name job (select in list)
        public string Name;
        // Description this job
        public string Description;

        public override string ToString()
        {
            return this.Name;
        }
    }
}
