﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CommonClasses
{
    public class Vault
    {
        // Computer name
        public static string CompName;
        // Date&Time Last Synchronization
        public DateTime LastSyncDateTime;
        // Full name (path) for Vault. It's unique name
        // my_comp.outlook2013.user1.my_contact
        // google.user.contacts
        // ora.ip_serv.sid.scheme.table||select_name
        public string StrongName { get; set; }
        // Human readable name for Vault that user see in UI. Exemple, google.user.contacts = "My Google Contacts"
        public string HumanReadableName { get; set; }
    }
}
