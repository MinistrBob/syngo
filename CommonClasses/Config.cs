﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Xml.Serialization;
using CommonClasses;
using log4net;

namespace SyngoForms
{
    [Serializable]
    public class Config
    {
        //
        private static readonly ILog Log = LogHelper.GetLoggerByType(typeof(Config));
        //
        public string ConfigFileName { get; set; }
        //
        //public event EventHandler ConfigChanged;
        // appConfigFileName = "C:\\MyGit\\Syngo\\bin\\Debug\\Syngo.xml"
        private static readonly string AppConfigFileName = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Syngo.xml");
        // userConfigFileName = "C:\\Users\\Ministr\\AppData\\Roaming\\Syngo\\Syngo.xml"
        private static readonly string UserConfigFileName =
            Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData),
                @"Syngo\Syngo.xml");
        // MainForm Position
        public int PosX { get; set; }
        public int PosY { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }


        public List<Job> JobList { get; set; }

        /*
        public SerializableFont Font { get; set; }
        
        [XmlIgnore]
        public Color BackColor { get; set; }
        */

        public Config()
        {
            PosX = 0;
            PosY = 0;
            Width = 900;
            Height = 600;
        }

        public void SaveToFile(string FileName)
        {
            using (StreamWriter sw = new StreamWriter(FileName, false, Encoding.GetEncoding(1251)))
            {
                XmlSerializer xs = new XmlSerializer(GetType());
                xs.Serialize(sw, this);
            }
        }

        public static Config LoadFromFile(string FileName)
        {
            try
            {
                if (!Directory.Exists(Path.GetDirectoryName(FileName)))
                    Directory.CreateDirectory(Path.GetDirectoryName(FileName));
                using (StreamReader sr = new StreamReader(FileName, Encoding.GetEncoding(1251)))
                {
                    XmlSerializer xs = new XmlSerializer(typeof(Config));
                    return xs.Deserialize(sr) as Config;
                }
            }
            catch
            {
                return new Config();
            }
        }

        public static Config LoadConfig()
        {
            Config conf;

            Log.Debug("----- LoadConfig begin");
            if (File.Exists(AppConfigFileName))
            {
                Log.Info("Get settings from file - " + AppConfigFileName);
                conf = Config.LoadFromFile(AppConfigFileName);
                conf.ConfigFileName = AppConfigFileName;
            }
            else if (File.Exists(UserConfigFileName))
            {
                Log.Info("Get settings from file - " + UserConfigFileName);
                conf = Config.LoadFromFile(UserConfigFileName);
                conf.ConfigFileName = UserConfigFileName;
            }
            else
            {
                Log.Info("Create new settings");
                conf = new Config();
            }
            Log.Debug("----- LoadAppSettings end");

            return conf;
        }

        public void SaveConfig()
        {
            Log.Debug("----- SaveConfig begin");

            if (!string.IsNullOrEmpty(ConfigFileName))
            {
                SaveToFile(ConfigFileName);
                Log.Info("Save settings to file - " + ConfigFileName);
            }
            else
            {
                try
                {
                    SaveToFile(AppConfigFileName);
                    Log.Info("Save settings to file - " + AppConfigFileName);
                }
                catch
                {
                    try
                    {
                        SaveToFile(UserConfigFileName);
                        Log.Info("Save settings to file - " + UserConfigFileName);
                    }
                    catch (Exception e)
                    {
                        Log.Info(string.Format(
                                               @"ERROR: Cannot write settings
                        Error text: {0}
                        Call stack: {1}", e.Message, e.StackTrace));
                    }
                }
            }
            Log.Debug("----- SaveConfig end");
        }

    }
}
