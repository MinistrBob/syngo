﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;


namespace CommonClasses
{
    public abstract class ProviderBase
    {
        // Current entity type
        public abstract string CurrentEntityType { get; set; }
        // Connect to Source where is Vaults
        public abstract void Connect();
        // Receiving entity types which are then synchronized (Contacts, Notes, Links, etc.)
        public abstract List<string> GetEntityTypes();
        // Get one particular store 
        public abstract Vault CreateVault();
        // Show Form for Login and Add Settings
        public abstract void ShowLoginForm();

    }
}
