﻿using log4net;
using System;
using System.Diagnostics;
using System.IO;
using System.Threading;
using System.Windows.Forms;

namespace CommonClasses
{
    public static class LogHelper
    {
        public static void LogConfigure()
        {
            // in .config write string
            // file type="log4net.Util.PatternString" value="%property{LogFileName}.txt" /

            //log4net.GlobalContext.Properties["LogFileName"] = @"E:\\file1"; //log file path
            log4net.Config.XmlConfigurator.Configure();
        }

        public static ILog GetLoggerByType(Type type)
        {
            return LogManager.GetLogger(type);
        }

        public static ILog GetLoggerByName(string name)
        {
            return LogManager.GetLogger(name);
        }
        /// <summary>
        /// Write text Unhandled Exception in special file
        /// </summary>
        public static void LogUnhandledException(Exception exception)
        {
            string msg = string.Format(
                        @"An unexpected error occurred and the program was closed.
                        Error text: {0}
                        Call stack: {1}", exception.Message, exception.StackTrace);

            // Пишем файл с ошибкой и стеком в папку откуда запускается программа, если не получается - в папку %TEMP%
            try
            {
                File.WriteAllText(
                        Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "!!!SyngoForms_exception.txt"),
                        DateTime.Now + "\r\n" + msg, System.Text.Encoding.GetEncoding(1251));
            }
            catch (Exception)
            {

                try
                {
                    File.WriteAllText(
                        Path.Combine(Path.GetTempPath(), "!!!SyngoForms_exception.txt"),
                        DateTime.Now + "\r\n" + msg, System.Text.Encoding.GetEncoding(1251));
                }
                catch (Exception)
                {
                    MessageBox.Show("Error write file Unexpected Error", "Unable to write file !!!SyngoForms_exception.txt", MessageBoxButtons.OK,
                MessageBoxIcon.Error);

                }
            }
            
            ILog log = LogHelper.GetLoggerByName("UnhandledException");
            log.Fatal(msg, exception);

            MessageBox.Show("Unexpected Error", "An unexpected error occurred and the program was closed.", MessageBoxButtons.OK,
                MessageBoxIcon.Error);

            /*
            try
            {
                if (!EventLog.SourceExists("SyngoForms.exe"))
                    EventLog.CreateEventSource("SyngoForms.exe", "Application");
                EventLog el = new EventLog("Application");
                el.Source = "SyngoForms.exe";
                el.WriteEntry(msg, EventLogEntryType.Error, 999, 0);
                el.Close();
            }
            catch
            {
            }
            */
        }

    }
}
