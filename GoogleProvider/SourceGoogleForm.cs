﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace GoogleProvider
{
    public partial class SourceGoogleForm : UserControl
    {
        private ProviderGoogle thisProviderGmail;
        public SourceGoogleForm(ProviderGoogle pg)
        {
            thisProviderGmail = pg;

            InitializeComponent();

            tbGoogleAccount.Text = "mail@gmail.com";

            foreach (string et in pg.GetEntityTypes())
            {
                cbEntetiesTypes.Items.Add(et);
            }
            cbEntetiesTypes.SelectedIndex = 0;
        }

        private void tbGoogleAccount_Validating(object sender, CancelEventArgs e)
        {
            // Validate login as email
            Regex re = new Regex(@"^(?'id'[a-z0-9\'\%\._\+\-]+)@(?'domain'[a-z0-9\'\%\._\+\-]+)\.(?'ext'[a-z]{2,6})$");
            if (!re.IsMatch(tbGoogleAccount.Text))
            {
                errorProvider1.SetError(tbGoogleAccount, "Login name must be E-Mail format xxx@xxx.xxx");
                e.Cancel = true;
                return;
            }
            // login is true
            errorProvider1.SetError(tbGoogleAccount, "");
        }

        private void button1_Click(object sender, EventArgs e)
        {
            thisProviderGmail.sourceGoogle.LoginUserName = tbGoogleAccount.Text;
            thisProviderGmail.CurrentEntityType = cbEntetiesTypes.SelectedItem.ToString();
            // Close window
            ((Form)this.TopLevelControl).Close();
        }
    }
}
