﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CommonClasses;

namespace GoogleProvider
{
    public class SourceGoogle : Source
    {
        public override string LoginUserName { get; set; }
        public override string LoginUserPassword { get; set; }
    }
}
