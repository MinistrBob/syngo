﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using CommonClasses;
using Google.Apis.Auth.OAuth2;
using Google.Apis.Calendar.v3;
using Google.Apis.Calendar.v3.Data;
using Google.Apis.Util.Store;
using Google.Contacts;
using Google.Documents;
using Google.GData.Client;
using Google.GData.Client.ResumableUpload;
using Google.GData.Contacts;
using Google.GData.Documents;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Runtime.InteropServices;
using System.Threading;
using System.Windows.Forms;
using log4net;


namespace GoogleProvider
{
    public class ProviderGoogle : ProviderBase
    {
        private static readonly ILog Log = LogHelper.GetLoggerByType(typeof(ProviderGoogle));

        public SourceGoogle sourceGoogle = new SourceGoogle();
        //public override string LoginUserName { get; set; }
        //public override string LoginUserPassword { get; set; }
        public override string CurrentEntityType { get; set; }

        public ContactsRequest ContactsRequest { get; private set; }

        private SourceGoogleForm providerGmailLoginForm;

        public ProviderGoogle()
        {
            this.providerGmailLoginForm = new SourceGoogleForm(this);
        }

        enum EntityTypes
        {
            Contacts,
            Schedules,
            Notes
        }

        public override void Connect()
        {
            //return false;
            Log.Debug("----- Connect begin");
            Log.Info("Connecting to Google...");


            //OAuth2 for all services
            List<String> scopes = new List<string>();

            //Contacts-Scope
            scopes.Add("https://www.google.com/m8/feeds");
            // calendar scope
            scopes.Add(CalendarService.Scope.Calendar);

            //take user credentials
            UserCredential credential;

            //load client secret from ressources
            byte[] jsonSecrets = Properties.Resources.client_secrets;

            //using (var stream = new FileStream(Application.StartupPath + "\\client_secrets.json", FileMode.Open, FileAccess.Read))
            using (var stream = new MemoryStream(jsonSecrets))
            {
                FileDataStore fDS = new FileDataStore(AppDomain.CurrentDomain.BaseDirectory, true);

                GoogleClientSecrets clientSecrets = GoogleClientSecrets.Load(stream);

                credential = GoogleOauth2WebAuthorizationBroker.AuthorizeAsync(
                                clientSecrets.Secrets,
                                scopes.ToArray(),
                                sourceGoogle.LoginUserName,
                                CancellationToken.None,
                                fDS).
                                Result;

                var initializer = new Google.Apis.Services.BaseClientService.Initializer();
                initializer.HttpClientInitializer = credential;

                OAuth2Parameters parameters = new OAuth2Parameters
                {
                    ClientId = clientSecrets.Secrets.ClientId,
                    ClientSecret = clientSecrets.Secrets.ClientSecret,

                    // Note: AccessToken is valid only for 60 minutes
                    AccessToken = credential.Token.AccessToken,
                    RefreshToken = credential.Token.RefreshToken
                };
//Logger.Log(Application.ProductName, EventType.Information);
                RequestSettings settings = new RequestSettings(
                    Application.ProductName, parameters);


                    //ContactsRequest = new ContactsRequest(rs);
                    ContactsRequest = new ContactsRequest(settings);


                /*
                //Obsolete, because no notes sync anymore:
                if (SyncNotes)
                {
                    //DocumentsRequest = new DocumentsRequest(rs);
                    DocumentsRequest = new DocumentsRequest(settings);

                    //Instantiate an Authenticator object according to your authentication, to use ResumableUploader
                    //GDataCredentials cred = new GDataCredentials(credential.Token.AccessToken);
                    //GOAuth2RequestFactory rf = new GOAuth2RequestFactory(null, Application.ProductName, parameters);
                    //DocumentsRequest.Service.RequestFactory = rf;

                    authenticator = new OAuth2Authenticator(Application.ProductName, parameters);
                }
                if (SyncAppointments)
                {
                    //ContactsRequest = new Google.Contacts.ContactsRequest()
                    var CalendarRequest = new CalendarService(initializer);

                    //CalendarRequest.setUserCredentials(username, password);

                    calendarList = CalendarRequest.CalendarList.List().Execute().Items;

                    //Get Primary Calendar, if not set from outside
                    if (string.IsNullOrEmpty(SyncAppointmentsGoogleFolder))
                        foreach (var calendar in calendarList)
                        {
                            if (calendar.Primary != null && calendar.Primary.Value)
                            {
                                SyncAppointmentsGoogleFolder = calendar.Id;
                                break;
                            }
                        }

                    if (SyncAppointmentsGoogleFolder == null)
                        throw new Exception("Google Calendar not defined (primary not found)");

                    //EventQuery query = new EventQuery("https://www.google.com/calendar/feeds/default/private/full");
                    //Old v2 approach: EventQuery query = new EventQuery("https://www.googleapis.com/calendar/v3/calendars/default/events");
                    EventRequest = CalendarRequest.Events;
                }
                */

            }

            Log.Debug("----- Connect end");
        }

        public override List<string> GetEntityTypes()
        {
            return Enum.GetNames(typeof(EntityTypes)).ToList();
        }

        public override Vault CreateVault()
        {
            ShowLoginForm();


            //DEBUG
            Vault v = new Vault();
            return v;

        }

        public override void ShowLoginForm()
        {
            //ProviderGmailLoginForm providerLoginControl = new ProviderGmailLoginForm();
            //providerLoginControl.Dock = DockStyle.Fill;
            Form loginForm = new Form();
            loginForm.AutoSize = true;
            loginForm.AutoSizeMode = AutoSizeMode.GrowAndShrink;
            loginForm.Text = "Create vault from Google";
            //loginForm.Size = providerLoginControl.Size;
            loginForm.Controls.Add(providerGmailLoginForm);
            loginForm.ShowDialog();

        }

    }
}
