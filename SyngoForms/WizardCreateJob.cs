﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using WizardFormLib;

namespace SyngoForms
{
	/// <summary>
	/// Presnts a basic useage example for the WizardFormLib.
	/// </summary>
	public partial class WizardCreateJob : WizardFormLib.WizardFormBase
	{
		WizardPage1		page1	= null;
		WizardPage2a	page2a	= null;
		WizardPage2b	page2b	= null;
		WizardPage3		page3	= null;
		WizardPage4		page4	= null;
		WizardPage5		page5	= null;

		//--------------------------------------------------------------------------------
		/// <summary>
		/// Constructor
		/// </summary>
		public WizardCreateJob()
		{
            InitializeComponent();

		    this.Text = "Wizard Create Job";
		}

		//--------------------------------------------------------------------------------
		/// <summary>
		/// Fired when the form is loaded
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void WizardExample_Load(object sender, EventArgs e)
		{
			// set our image panel metrics
			this.GraphicPanelImagePosition = WizardImagePosition.Right;
			//this.GraphicPanelImageResource = "WizardDemo.udplogo.png";
			this.GraphicPanelGradientColor = Color.DarkSlateBlue;

			// if you don't need a given button, you can hide it here
			this.ButtonHelpHide = true;
			this.ButtonStartHide = true;

			// add handlers for the buttons
			this.buttonBack.Click	+= new System.EventHandler(this.buttonBack_Click);
			this.buttonNext.Click	+= new System.EventHandler(this.buttonNext_Click);
			this.buttonCancel.Click	+= new System.EventHandler(this.buttonCancel_Click);
			this.buttonHelp.Click	+= new System.EventHandler(this.buttonHelp_Click);
			this.buttonStart.Click	+= new System.EventHandler(this.buttonStart_Click);

			// create the wizard pages we need
			page1	= new WizardPage1(this, WizardPageType.Start);
			page2a	= new WizardPage2a(this);
			page2b	= new WizardPage2b(this);
			page3	= new WizardPage3(this);
			page4	= new WizardPage4(this);
			page5	= new WizardPage5(this, WizardPageType.Stop);

			// add a handler that lets us know when a page has been activated
			page1.WizardPageActivated	+= new WizardPageActivateHandler(WizardPageActivated);
			page2a.WizardPageActivated	+= new WizardPageActivateHandler(WizardPageActivated);
			page2b.WizardPageActivated	+= new WizardPageActivateHandler(WizardPageActivated);
			page3.WizardPageActivated	+= new WizardPageActivateHandler(WizardPageActivated);
			page4.WizardPageActivated	+= new WizardPageActivateHandler(WizardPageActivated);
			page5.WizardPageActivated	+= new WizardPageActivateHandler(WizardPageActivated);
	
			// make sure all of the necessary pages have a "next" page
			page1.AddNextPage(page2a);
			page1.AddNextPage(page2b);
			page2a.AddNextPage(page3);
			page2b.AddNextPage(page3);
			page3.AddNextPage(page4);
			page4.AddNextPage(page5);

			// start the wizard
			StartWizard();
		}

		//--------------------------------------------------------------------------------
		/// <summary>
		/// Fired when a wizard page is activated (made visible)
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		void WizardPageActivated(object sender, WizardPageActivateArgs e)
		{
			PaintTitle();
			this.buttonBack.Enabled = (e.ActivatedPage.WizardPageType != WizardPageType.Start);
			if (e.ActivatedPage.WizardPageType == WizardPageType.Stop)
			{
				this.buttonNext.Text = "Finished";
			}
			else
			{
				this.buttonNext.Text = "Next >";
			}
		}

		//--------------------------------------------------------------------------------
		/// <summary>
		/// Fired when the back button is clicked
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void buttonBack_Click(object sender, EventArgs e)
		{
			// tell the page chain to go to the previous page
			WizardPage currentPage = PageChain.GoBack();
			// raise the page change event (this currently does nothing but lets the 
			// base class know when the active page has changed
			Raise_WizardPageChangeEvent(new WizardPageChangeArgs(currentPage, WizardStepType.Previous));
		}

		//--------------------------------------------------------------------------------
		/// <summary>
		/// Fired when the Next button is clicked
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void buttonNext_Click(object sender, EventArgs e)
		{
			// if the current page (before changing) is the last page in the wizard, 
			// take steps to close the wizard
			if (PageChain.GetCurrentPage().WizardPageType == WizardPageType.Stop)
			{
				// call the central SaveData method (which calls the SaveData 
				// method in each page in the chain
				if (PageChain.SaveData() == null)
				{
					// and if everything is okay, close the wizard form
					this.Close();
				}
			}
			// otherwise, move to the next page in the chain, and let the base class know
			else
			{
				WizardPage currentPage = PageChain.GoNext(PageChain.GetCurrentPage().GetNextPage());
				Raise_WizardPageChangeEvent(new WizardPageChangeArgs(currentPage, WizardStepType.Next));
			}
		}

		//--------------------------------------------------------------------------------
		/// <summary>
		/// Fired when the user clicks the Cancel button
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void buttonCancel_Click(object sender, EventArgs e)
		{
			this.Close();
		}

		//--------------------------------------------------------------------------------
		/// <summary>
		/// Fired when the user clicks the Help button
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void buttonHelp_Click(object sender, EventArgs e)
		{
			MessageBox.Show("Not implemented yet.");
		}

		//--------------------------------------------------------------------------------
		/// <summary>
		/// Fired when the user clicks the Start button (to return to the first wizard 
		/// page).
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void buttonStart_Click(object sender, EventArgs e)
		{
			WizardPage currentPage = PageChain.GoFirst();
			// raise the page change event - this currently does nothing but lets the 
			// base class know when the active page has changed
			Raise_WizardPageChangeEvent(new WizardPageChangeArgs(currentPage, WizardStepType.Previous));
		}

	}
}



