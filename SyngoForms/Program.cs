﻿using log4net;
using System;
using System.Diagnostics;
using System.IO;
using System.Threading;
using System.Windows.Forms;
using CommonClasses;

namespace SyngoForms
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            LogHelper.LogConfigure();
            AppDomain.CurrentDomain.UnhandledException += new UnhandledExceptionEventHandler(CurrentDomain_UnhandledException);
            Application.SetUnhandledExceptionMode(UnhandledExceptionMode.CatchException);
            Application.ThreadException += new System.Threading.ThreadExceptionEventHandler(Application_ThreadException);
			
			// Проверка есть ли запущеные копии программы, реализована с помощью мьютекса
            bool onlyInstance;
            // Создаем мьютекс
            using (Mutex mtx = new Mutex(true, "SyngoForms.exe", out onlyInstance))
            {
                // Если он создается, значит ни одной копии еще не запущено - просто запускаем.
                if (onlyInstance)
                {
                    RunSyngoForms();
                }
                // Если он НЕ создается, значит уже есть запущеные экземпляры - спрашиваем юзера что делать
                else
                {
                    if (DialogResult.Yes == MessageBox.Show(
                                           "Application \"SyngoForms \" is already running, you want to start another copy?",
                                           "Message",
                                           MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation))
                    {
                        RunSyngoForms();
                    }

                }
            }

        }
		
		private static void RunSyngoForms()
        {
            try
            {
                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);
                Application.Run(new MainForm());
            }
            catch (Exception ex)
            {
                LogHelper.LogUnhandledException(ex);
            }
        }
		
        // ---------------------------------------------------------------------------------------------------------------
        static void Application_ThreadException(object sender, System.Threading.ThreadExceptionEventArgs e)
        {
            LogHelper.LogUnhandledException(e.Exception);
        }

        // ---------------------------------------------------------------------------------------------------------------

        static void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            LogHelper.LogUnhandledException(e.ExceptionObject as Exception);
        }

    }
}
