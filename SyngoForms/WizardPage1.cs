﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using WizardFormLib;

namespace SyngoForms
{
	public partial class WizardPage1 : WizardFormLib.WizardPage
	{
		//--------------------------------------------------------------------------------
		/// <summary>
		/// Constructor that assumes the page type is "intermediate"
		/// </summary>
		/// <param name="parent">The parent WizardFormBase-derived form</param>
		public WizardPage1(WizardFormBase parent) 
					: base(parent)
		{
			InitPage();
		}

		//--------------------------------------------------------------------------------
		/// <summary>
		/// Constructor that allows the programmer to specify the page type. In 
		/// the case of the sample app, we use this constructor for this oject, 
		/// and specify it as the start page.
		/// </summary>
		/// <param name="parent">The parent WizardFormBase-derived form</param>
		/// <param name="pageType">The type of page this object represents (start, intermediate, or stop)</param>
		public WizardPage1(WizardFormBase parent, WizardPageType pageType) 
					: base(parent, pageType)
		{
			InitPage();
		}

		//--------------------------------------------------------------------------------
		/// <summary>
		/// This method serves as a common constructor initialization location, 
		/// and serves mainly to set the desired size of the container panel in 
		/// the wizard form (see WizardFormBase for more info).  I didn't want 
		/// to do this here but it was the only way I could get the form to 
		/// resize itself appropriately - it needed to size itself according 
		/// to the size of the largest wizard page.
		/// </summary>
		public void InitPage()
		{
			ButtonStateNext &= ~WizardButtonState.Enabled;
			InitializeComponent();
			base.Size = this.Size;
			//base.Title = this.Title;
			//base.Subtitle = this.Subtitle;
			this.ParentWizardForm.DiscoverPagePanelSize(this.Size);
			//this.ParentWizardForm.EnableNextButton(false);
		}

		//--------------------------------------------------------------------------------
		/// <summary>
		/// Overriden method that allows this wizard page to save page-specific data.
		/// </summary>
		/// <returns>True if the data was saved successfully</returns>
		public override bool SaveData()
		{
			return true;
		}

		//--------------------------------------------------------------------------------
		/// <summary>
		/// This is an overriden method that performs special processing when 
		/// it's time to select the next page to display. In the case of our 
		/// Page 1, a different "next" page is displayed depending on which 
		/// radio button has been selected.
		/// </summary>
		/// <returns>The new current page that we will show</returns>
		public override WizardPage GetNextPage()
		{
			// some volutary sanity checking
			if (NextPages.Count != 2)
			{
				throw new WizardFormException("Page 1 expects two \"next\" pages to be specified.");
			}
			// make a choice
			if (this.radioButton1.Checked)
			{
				return NextPages[0];
			}
			else
			{
				return NextPages[1];
			}
		}

		//--------------------------------------------------------------------------------
		/// <summary>
		/// Fired when the user selects one of the radio buttons. In our case the 
		/// "Next" button is disabled and the act of selecting one of the radio 
		/// buttons allows the user to proceed in the wizard.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void radioButton1_CheckedChanged(object sender, EventArgs e)
		{
			//this.ParentWizardForm.EnableNextButton(true);
			ButtonStateNext |= WizardButtonState.Enabled;
			ParentWizardForm.UpdateWizardForm(this);
		}

		//--------------------------------------------------------------------------------
		/// <summary>
		/// Fired when the user selects one of the radio buttons. In our case the 
		/// "Next" button is disabled and the act of selecting one of the radio 
		/// buttons allows the user to proceed in the wizard.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void radioButton2_CheckedChanged(object sender, EventArgs e)
		{
			//this.ParentWizardForm.EnableNextButton(true);
			ButtonStateNext |= WizardButtonState.Enabled;
			ParentWizardForm.UpdateWizardForm(this);
		}

	}
}
