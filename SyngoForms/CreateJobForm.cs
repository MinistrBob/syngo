﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using CommonClasses;

namespace SyngoForms
{
    public partial class CreateJobForm : Form
    {
        private List<Job> listJobs;
        public CreateJobForm()
        {
            InitializeComponent();
        }

        public CreateJobForm(List<Job> listJobs)
            : this()
        {
            this.listJobs = listJobs;
        }

        private void btSave_Click(object sender, EventArgs e)
        {
            Job job = new Job();
            job.Name = tbJobName.Text;
            job.Description = tbDescription.Text;

            listJobs.Add(job);

            this.Close();
        }


    }
}
