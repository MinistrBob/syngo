﻿using System.Collections.Generic;
using log4net;
using System;
using System.IO;
using System.Windows.Forms;
using CommonClasses;
using GoogleProvider;

namespace SyngoForms
{
    public partial class MainForm : Form
    {
        //
        private static readonly ILog Log = LogHelper.GetLoggerByType(typeof(MainForm));
        //
        public Config Config;

        public List<Job> JobList; 

        public MainForm()
        {
            Config = Config.LoadConfig();
            InitializeComponent();
            ApplyConfig();
        }

        private void ApplyConfig()
        {

            Left = Config.PosX;
            Top = Config.PosY;
            Width = Config.Width;
            Height = Config.Height;

            // Это временно
            JobList = Config.JobList;

            UpdateUI();

           

        }

        private void UpdateUI()
        {
            // Пока здесь ничего нет
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            

            //dat1.ToString(System.Globalization.CultureInfo.InvariantCulture)
        }

        private void MainForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            Config.PosX = Left;
            Config.PosY = Top;
            Config.Width = Width;
            Config.Height = Height;

            Config.JobList = JobList;

            Config.SaveConfig();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            ProviderGoogle pg = new ProviderGoogle();
            Vault v = pg.CreateVault();
            pg.Connect();

        }

        private void ShowProviderLoginForm()
        {
            
        }

        private void llCreateJob_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            WizardCreateJob form = new WizardCreateJob();
            form.ShowDialog();
            
            //CreateJobForm createJobForm = new CreateJobForm(JobList);
            //createJobForm.ShowDialog();

            //cmbJobList.Items.AddRange(JobList.ToArray());
        }


    }
}
