﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using WizardFormLib;

namespace SyngoForms
{
	public partial class WizardPage5 : WizardFormLib.WizardPage
	{
		public WizardPage5(WizardFormBase parent)
					:base(parent)
		{
			InitPage();
		}

		public WizardPage5(WizardFormBase parent, WizardPageType pageType) 
					: base(parent, pageType)
		{
			InitPage();
		}

		//--------------------------------------------------------------------------------
		/// <summary>
		/// This method serves as a common constructor initialization location, 
		/// and serves mainly to set the desired size of the container panel in 
		/// the wizard form (see WizardFormBase for more info).  I didn't want 
		/// to do this here but it was the only way I could get the form to 
		/// resize itself appropriately - it needed to size itself according 
		/// to the size of the largest wizard page.
		/// </summary>
		public void InitPage()
		{
			InitializeComponent();
			base.Size = this.Size;
			this.ParentWizardForm.DiscoverPagePanelSize(this.Size);

			// add a handler to let us know when the wizard form has been "started"
			this.ParentWizardForm.WizardFormStartedEvent += new WizardFormStartedHandler(ParentWizardForm_WizardFormStartedEvent);
		}

		//--------------------------------------------------------------------------------
		/// <summary>
		/// Fired when the wizard form has been started (and after all of the pages have 
		/// been added).
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		void ParentWizardForm_WizardFormStartedEvent(object sender, WizardFormStartedArgs e)
		{
			// center the groupbox container in the page. This should always work 
			// because the form is large enough to accomodate this wizard page.

			// get the size of the page panel
			Size parentPanel = this.ParentWizardForm.PagePanelSize;
			// calculate our x/y centers
			int x = (int)((parentPanel.Width - this.groupBox1.Width) * 0.5);
			int y = (int)((parentPanel.Height - this.groupBox1.Height) * 0.5);
			// move the container to its new location
			this.groupBox1.Location = new Point(x, y);
		}
	}
}
