﻿using System;
using System.Windows.Forms;
//using DevExpress.XtraEditors;

namespace SyngoForms
{
    public partial class frmConfig : Form
    {
        private MainForm _MainForm;
        public Config config;
        public frmConfig(MainForm MainForm)
        {
            InitializeComponent();
            this._MainForm = MainForm;
        }

        private void frmConfig_Load(object sender, EventArgs e)
        {
            if (listView1.SelectedIndices.Count == 0)
                listView1.Items[0].Selected = true;
            OnPageChanged();
            config = _MainForm.Config;
            //config = _MainForm.Config.ShallowCopy();
            this.configBindingSource.DataSource = config;
        }

        private Panel GetPanel(int index)
        {
            var controls = splitContainer1.Panel2.Controls.Find("pnlConfig" + index.ToString(), false);
            return (controls.Length > 0 ? controls[0] as Panel : null);
        }

        public virtual void OnPageChanged()
        {
            int _SelectedIndex = listView1.SelectedIndices.Count > 0 ? listView1.SelectedIndices[0] : -1;
            for (int i = 0; i < listView1.Items.Count; i++)
                GetPanel(i).Visible = (i == _SelectedIndex);
        }

        private void listView1_SelectedIndexChanged(object sender, EventArgs e)
        {
            OnPageChanged();
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            configBindingSource.EndEdit();
            _MainForm.Config = config;
            DialogResult = System.Windows.Forms.DialogResult.OK;
        }


    }
}
